import json
import datetime

import gocd
import jira
import confluence
import googlecalendar

def main():
    jira_client = jira.Client(
        "jzantinge@oanda.com",
        "iTDKdPJKyxBN6I3URiRO8C34",
        (
            'project = "Backend Applications" AND '
            '(type = "Support request" OR status = Triage) AND '
            'labels = Backend-Execution AND '
            '('
                'status not in (Resolved, Closed) OR '
                'status in (Resolved, Closed) AND '
                'updatedDate > -7d'
            ') '
            'ORDER BY Rank ASC'
        ),
            (
                'project = BREL AND '
                'resolution = Unresolved AND '
                'labels = Backend-Execution '
                'ORDER BY priority DESC, updated DESC'
            )
        )
    # gocd_client = gocd.Client("qauser", "rabAfud4")
    confluence_client = confluence.Client(
        "jzantinge@oanda.com",
        "iTDKdPJKyxBN6I3URiRO8C34",
        "52625780",
        "<ac:layout-cell><h4><strong>Backend Execution Triage Schedule</strong></h4>",
        "/workspace/git/bootler/etc/triageschedule.template"
    )
    googlecalendar_client = googlecalendar.Client(
        "bootler",
        "/workspace/git/bootler/etc/client_secret.json",
        "/workspace/git/bootler/builds/credentials.json"
    )

    googlecalendar_client.get_vacationers()

    # tickets_by_status, tickets_by_assignee = jira_client.get_release_tickets()
    # print json.dumps(tickets_by_status, indent=4)
    # gocd_client.get_pipelines(["pricing", "hedger"])
    # print json.dumps(gocd_client.get_pipelines(["pricing", "hedger"]), indent=4)
    # print confluence_client.get_user_key("jzantinge")
    # confluence_client.update_triage_schedule(
    #     [
    #         {
    #             "username": "cfung",
    #             "week_start": datetime.date(2018, 5, 7)
    #         },
    #         {
    #             "username": "natalia",
    #             "week_start": datetime.date(2018, 5, 14)
    #         },
    #         {
    #             "username": "wzhang",
    #             "week_start": datetime.date(2018, 5, 21)
    #         },
    #         {
    #             "username": "jzantinge",
    #             "week_start": datetime.date(2018, 5, 28)
    #         },
    #         {
    #             "username": "cfung",
    #             "week_start": datetime.date(2018, 6, 4)
    #         },
    #         {
    #             "username": "natalia",
    #             "week_start": datetime.date(2018, 6, 11)
    #         },
    #         {
    #             "username": "wzhang",
    #             "week_start": datetime.date(2018, 6, 18)
    #         },
    #         {
    #             "username": "jzantinge",
    #             "week_start": datetime.date(2018, 6, 25)
    #         }
    #     ]
    # )

main()
