# Name of the egg file
BOOTLER_EGG_NAME := bootler-$(BUILD_VERSION)-py$(PYTHON_VERSION).egg

TARGETS := $(INSTALLPATH)/dist/$(BOOTLER_EGG_NAME)

BOOTLER_EGG_DIRECTORIES := $(d)

$(INSTALLPATH)/dist/$(BOOTLER_EGG_NAME)_CMD := \
    $(call python_egg_cmd, $(BOOTLER_EGG_DIRECTORIES), BUILD_VERSION)

$(INSTALLPATH)/dist/$(BOOTLER_EGG_NAME)_DEPS := \
    $(shell $(FIND) $(d) -name '*.py')
