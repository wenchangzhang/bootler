import os
import json
import requests

from sets import Set
from time import sleep
from datetime import *
from heapq import heappush, heappop
from dateutil.parser import parse

from slackclient import SlackClient

from af.config.config import Config

import gocd
import phabricator
import jira
import confluence
from schedule import Schedule
from team import Team

UNKNOWN_USER = "a ghost :ghosttwirl:"

RELEVANT_RELEASE_STATUSES = [
    "Dev-Assigned",
    "Dev-Approval",
    "Int-Testing",
    "Dev-Clonepending"
]

TOPIC = "<==Important links are pinned. Triage Person: {}"

REVIEW = "{} [*{}*] <https://phabricator.dev.oanda.com/D{}|{}> by {}\n"
ACCEPTED_REVIEW = (
    "{} [*{}*] <https://phabricator.dev.oanda.com/D{}|{}> by {} [*Reviewed by*: {}]\n"
)

OPEN_REVIEWS = (
    "*{} revision{} need{} to be reviewed!*\n{}\n"
    "*{} revision{} ha{} been rejected!*\n{}\n"
    "*{} revision{} need{} one more* (or need{} to be closed)!\n{}"
)

MEMBER_TRIAGE_TICKETS = "*{} triage ticket{}* {} owned by {}\n"

TRIAGE = (
    "{} is the *Triage Person* "
    "[*<https://oandacorp.atlassian.net/wiki/spaces/TXS/pages/52625780/"
    "Triage+Schedule|Schedule>*]\n\n"
    "There are *{} triage ticket{} \"To Do\"*: "
    "<https://oandacorp.atlassian.net/secure/RapidBoard.jspa?rapidView=429|"
    "Triage Board>\n"
    "{}"
)

RELEASE_SUMMARY = "<{}|R{}> {} - <{}|{}> is going to *{}*\n"

RELEASES = (
    "*Upcoming Releases*\n"
    "{}"
)

RELEASE_WORK = (
    "*These releases need attention!*\n"
    "{}"
)

RELEASE_UPDATES = (
    "@here These releases now need some attention!\n"
    "{}"
)

DAILY_MESSAGE = (
    "Hello there {} team! Here's what's on the docket for "
    "today:\n\n"
    "{}\n"
    "*GoCD Build Status*"
)

DAILY_POST_LUNCH_MESSAGE = (
    "I hope lunch was delicious, here are the reviews that are still open:\n"
    "{}\n"
    "GoCD Build Status"
)

DAILY_STAND_UP = "*_Time to Stand Up!_* {}"

NEW_REVIEWS_MESSAGE = (
    "@here: I've found {} new review{}!\n"
    "{}"
)

UPDATED_REVIEWS_MESSAGE=(
    "@here "
    "{}"
)

UPDATED_MESSAGE = (
    "{} review{} ha{} been updated:\n"
    "{}\n"
)
REJECTED_MESSAGE = (
    "{} review{} ha{} been rejected:\n"
    "{}"
)
NEEDS_SECOND_MESSAGE = (
    "{} review{} now need{} a second:\n"
    "{}"
)

PIPELINE_GROUP_GOOD = "*{}* is good."
BUSTED_STAGE = "*{}* broke the *{}* stage of *{}*"

PIPELINE_GOOD = "*{}* is now good."
PIPELINE_BUILDING = "*{}* is now building."
PIPELINE_BUSTED = "*{}* is now busted (thanks {})."
PIPELINE_STAGE_LINK = (
    "<"
    "https://gocd-master.svc.ri.oanda.com/go/pipelines/"
    "{pipeline}/{instance}/{stage}/{counter}|"
    "{pipeline}"
    ">"
)

BUILD_STATUS_CHANGE_MESSAGE = "I've noticed the *build status* has changed!"

size_map = {
    "minuscule": ":heart_eyes_cat:",
    "tiny": ":smile_cat:",
    "small": ":smiley_cat:",
    "medium": ":smirk_cat:",
    "large": ":crying_cat_face:",
    "massive": ":scream_cat:",
    "outrageous": ":skull:",
    "unknown": ":japanese_ogre:"
}

class Bootler(object):
    def __init__(self, config):
        self.bootler = SlackClient(config.slack.token)

        self.team_name = config.teamName
        self.daily_stand_up_message = config.standUp.message
        self.daily_stand_up_time = config.standUp.time

        self.review_channel = config.slack.reviewChannel
        self.triage_channel = config.slack.triageChannel

        if not self.bootler.rtm_connect(with_team_state=False):
            print "Bootler vanished."

            exit(0)

        self.bootler_id = self.bootler.api_call("auth.test")["user_id"]

        self.phabricator = phabricator.Client(
            token=config.phabricator.token,
            query_key=config.phabricator.queryKey
        )

        self.jira = jira.Client(
            config.jira.user,
            config.jira.token,
            config.jira.triageQuery,
            config.jira.releaseQuery,
        )

        # self.confluence = confluence.Client(
        #     config.confluence.user,
        #     config.confluence.token,
        #     config.confluence.triageSchedulePageID,
        #     config.confluence.contentMarker,
        #     config.confluence.tableTemplateFile
        # )

        self.gocd = gocd.Client(
            config.gocd.username,
            config.gocd.password,
            config.gocd.groups.translate(None, ' ').split(",")
        )

        self.last_known_reviews = None
        self.last_pipeline_status = None
        self.last_release_status = None

        self.team = Team(
            [
                {
                    "first_name": member['firstName'],
                    "last_name": member['lastName'],
                    "phabricator_name": member['phabricatorName'],
                    "slack_name": member['slackName'],
                    "gender": member['gender'],
                    "exclude_from_triage": member['excludeFromTriage']
                }
                for member in config.team
            ],
            parse(config.triageReferenceDate)
        )

        self.message_schedule = Schedule(
            [
                # {
                #     "type": "Weekly",
                #     "time": "9:00am",
                #     "day of the week": "monday",
                #     "event": self.triage_person_change
                # },
                # {
                #     "type": "Daily",
                #     "include_weekends": False,
                #     "time": "6:00am",
                #     "event": self.update_triage_schedule
                # },
                {
                    "type": "Daily",
                    "include_weekends": False,
                    "time": "9:30am",
                    "event": self.daily_summary
                },
                {
                    "type": "Daily",
                    "include_weekends": False,
                    "time": "1:30pm",
                    "event": self.daily_post_lunch_summary
                },
                {
                    "type": "Daily",
                    "include_weekends": False,
                    "time": self.daily_stand_up_time,
                    "event": self.daily_stand_up
                },
                # {
                #     "type": "Repeated",
                #     "frequency": 10,
                #     "event": self.daily_stand_up
                # },
                {
                    "type": "Repeated",
                    "frequency": 10,
                    "event": self.check_for_review_changes
                },
                {
                    "type": "Repeated",
                    "frequency": 10,
                    "event": self.check_for_build_updates
                # },
                # {
                #     "type": "Repeated",
                #     "frequency": 300,
                #     "event": self.check_for_release_updates
                }
            ]
        )

    def serve(self, start, end):
        self.message_schedule.trigger_events(start, end)

    def get_channel_id(self, name):
        result = self.bootler.api_call(
            "conversations.list",
            types="public_channel,private_channel"
        )

        next_cursor = None
        while True:
            if not result["ok"]:
                print result
                break

            for channel in result["channels"]:
                if "backend" in channel["name"]:
                    print channel["name"]

                if channel["name"] == name:
                    return channel["id"]

            next_cursor = None
            if "response_metadata" in result:
                metadata = result["response_metadata"]
                if "next_cursor" in metadata and metadata["next_cursor"]:
                    next_cursor = metadata["next_cursor"]

            if next_cursor is None:
                break

            sleep(1)

            result = self.bootler.api_call(
                "conversations.list",
                cursor=next_cursor,
                types="public_channel,private_channel"
            )

        return None

    def create_review_summary(self, review_format, reviews):
        summary = ""
        for review in reviews:
            author = self.team.get_member_by_phabricator_name(
                review["author"]
            )

            summary += review_format.format(
                size_map[review["size"]],
                review["repo"] if review["repo"] != "???" else ":ghosttwirl:",
                review["id"],
                (
                    review["title"]
                    if len(review["title"]) < 50 else
                    "{}...".format(review["title"][:50])
                ),
                author.slack_name if author is not None else UNKNOWN_USER,
                ", ".join(review["reviewers"])
            )

        return summary

    def create_triage_summary(
        self,
        tickets_by_status,
        tickets_by_assignee
    ):
        to_do_count = (
            len(tickets_by_status["To Do"])
            if "To Do" in tickets_by_status else
            None
        )

        triage_member_summary = ""
        for assignee, tickets in tickets_by_assignee.items():
            team_member = self.team.get_member_by_phabricator_name(assignee)

            triage_member_summary += MEMBER_TRIAGE_TICKETS.format(
                len(tickets),
                "s" if len(tickets) != 1 else "",
                "are" if len(tickets) != 1 else "is",
                (
                    team_member.slack_name
                    if team_member is not None else
                    "@{}".format(assignee)
                )
            )

        triage_summary = TRIAGE.format(
            self.team.triage_person,
            to_do_count if to_do_count is not None else UNKNOWN_USER,
            "s" if to_do_count is not None or to_do_count > 1 else "",
            triage_member_summary
        )

        return triage_summary

    def create_release_summary(self, releases):
        summary = ""

        for status, tickets in releases.items():
            for raven_id, ticket in tickets.items():
                summary += RELEASE_SUMMARY.format(
                    ticket["raven_link"],
                    raven_id,
                    "*{}*".format(ticket["status"])
                    if ticket["status"] in RELEVANT_RELEASE_STATUSES else
                    "{}".format(ticket["status"]),
                    ticket["jira_link"],
                    ticket["summary"],
                    ticket["environment"]
                )

        return summary

    def create_release_work_summary(self, releases):
        summary = ""

        for status, tickets in releases.items():
            for raven_id, ticket in tickets.items():
                if ticket["status"] not in RELEVANT_RELEASE_STATUSES:
                    continue

                summary += RELEASE_SUMMARY.format(
                    ticket["raven_link"],
                    raven_id,
                    "*{}*".format(ticket["status"])
                    if ticket["status"] in RELEVANT_RELEASE_STATUSES else
                    "{}".format(ticket["status"]),
                    ticket["jira_link"],
                    ticket["summary"],
                    ticket["environment"]
                )

        return summary

    def create_gocd_attachments(self):
        attachments = []

        try:
            pipelines_by_group = self.gocd.get_pipelines()
        except Exception as e:
            print "Failed to retrieve pipeline status.\n", str(e)

            return

        # print json.dumps(pipelines_by_group, indent=4)

        for group_name, group in pipelines_by_group.items():
            group_is_good = True
            for pipeline_name, pipeline in group.items():
                pipeline_building = False
                for stage_name, stage_info in pipeline["stages"].items():
                    stage_result = stage_info["result"]
                    if stage_result == "unknown":
                        pipeline_building = True
                        group_is_good = False

                        message = PIPELINE_BUILDING.format(
                            pipeline_name
                        )

                        attachments.append(
                            {
                                "text": message,
                                "fallback": message,
                                "color": "warning"
                            }
                        )
                        break
                    elif stage_result == "N/A":
                        continue
                    elif stage_result != "passed":
                        group_is_good = False

                        breaker = self.team.get_member_by_phabricator_name(
                            pipeline["changes"][0]["name"]
                        )
                        message = BUSTED_STAGE.format(
                            breaker.slack_name
                            if breaker is not None else
                            UNKNOWN_USER,
                            stage_name,
                            PIPELINE_STAGE_LINK.format(
                                pipeline=pipeline_name,
                                instance=pipeline["counter"],
                                stage=stage_name,
                                counter=stage_info["counter"]
                            )
                        )

                        attachments.append(
                            {
                                "text": message,
                                "fallback": message,
                                "color": "danger"
                            }
                        )

                        break

            if group_is_good:
                message = PIPELINE_GROUP_GOOD.format(group_name)

                attachments.append(
                    {
                        "text": message,
                        "fallback": message,
                        "color": "good"
                    }
                )

        return attachments

    def daily_summary(self):
        reviews = self.phabricator.query_revisions()
        needs_review = reviews["Needs Review"]
        accepted = reviews["Accepted"]
        rejected = reviews["Rejected"]

        needs_review_info = self.create_review_summary(REVIEW, needs_review)
        rejected_info = self.create_review_summary(REVIEW, rejected)
        accepted_info = self.create_review_summary(ACCEPTED_REVIEW, accepted)

        # triage_tickets_by_status, triage_tickets_by_assignee = (
        #     self.jira.get_triage_tickets()
        # )

        # triage_info = self.create_triage_summary(
        #     triage_tickets_by_status,
        #     triage_tickets_by_assignee
        # )

        # release_tickets_by_status, _ = self.jira.get_release_tickets()

        # release_info = (
        #     self.create_release_summary(release_tickets_by_status)
        #     if release_tickets_by_status is not None else
        #     ""
        # )

        print reviews

        self.bootler.api_call(
            "chat.postMessage",
            channel=self.review_channel,
            as_user=True,
            link_names=True,
            text=DAILY_MESSAGE.format(
                self.team_name,
                OPEN_REVIEWS.format(
                    len(needs_review),
                    "s" if len(needs_review) != 1 else "",
                    "s" if len(needs_review) == 1 else "",
                    needs_review_info,
                    len(rejected),
                    "s" if len(rejected) != 1 else "",
                    "s" if len(rejected) == 1 else "ve",
                    rejected_info,
                    len(accepted),
                    "s" if len(accepted) != 1 else "",
                    "s" if len(accepted) == 1 else "",
                    "s" if len(accepted) == 1 else "",
                    accepted_info
                )
            ),
            attachments=self.create_gocd_attachments()
        )

    def daily_post_lunch_summary(self):
        reviews = self.phabricator.query_revisions()
        needs_review = reviews["Needs Review"]
        rejected = reviews["Rejected"]
        accepted = reviews["Accepted"]

        needs_review_info = self.create_review_summary(REVIEW, needs_review)
        rejected_info = self.create_review_summary(REVIEW, rejected)
        accepted_info = self.create_review_summary(ACCEPTED_REVIEW, accepted)

        # release_tickets_by_status, _ = self.jira.get_release_tickets()

        # release_info = (
        #     self.create_release_work_summary(release_tickets_by_status)
        #     if release_tickets_by_status is not None else
        #     ""
        # )

        self.bootler.api_call(
            "chat.postMessage",
            channel=self.review_channel,
            as_user=True,
            link_names=True,
            text=DAILY_POST_LUNCH_MESSAGE.format(
                OPEN_REVIEWS.format(
                    len(needs_review),
                    "s" if len(needs_review) > 1 else "",
                    "s" if len(needs_review) == 1 else "",
                    needs_review_info,
                    len(rejected),
                    "s" if len(rejected) != 1 else "",
                    "s" if len(rejected) == 1 else "ve",
                    rejected_info,
                    len(accepted),
                    "s" if len(accepted) > 1 else "",
                    "s" if len(accepted) == 1 else "",
                    "s" if len(accepted) == 1 else "",
                    accepted_info
                )
            ),
            attachments=self.create_gocd_attachments()
        )

    def daily_stand_up(self):
        self.bootler.api_call(
            "chat.postMessage",
            channel=self.review_channel,
            as_user=True,
            link_names=True,
            text=self.daily_stand_up_message.format(
                " ".join(self.team.get_slack_names(True))
            )
        )

    def triage_person_change(self):
        self.team.update_triage_person([])

        print(self.bootler.api_call(
            "conversations.setTopic",
            channel=self.get_channel_id(self.triage_channel),
            topic=TOPIC.format(self.team.triage_person)
        ))

    def update_triage_schedule(self):
        self.confluence.update_triage_schedule(
            self.team_name,
            self.team.get_triage_schedule()
        )

    def check_for_review_changes(self):
        latest_reviews = self.phabricator.query_revision_ids()
        if latest_reviews is None:
            return

        if self.last_known_reviews is None:
            self.last_known_reviews = latest_reviews
            return

        last_known_reviews_set = Set(
            [key for key in self.last_known_reviews.keys()]
        )
        latest_reviews_set = Set([key for key in latest_reviews.keys()])

        # last_known_reviews_set -= Set([23228])

        new_review_ids = (
            latest_reviews_set - (last_known_reviews_set & latest_reviews_set)
        )

        print (
            "Checking for new reviews:\n"
            "known:  {}\n"
            "latest: {}\n"
            "new     {}".format(
                ",".join([str(id) for id in latest_reviews]),
                ",".join([str(id) for id in self.last_known_reviews]),
                ",".join([str(id) for id in new_review_ids])
            )
        )

        if new_review_ids:
            new_reviews = self.phabricator.get_reviews(new_review_ids)
            new_review_info = self.create_review_summary(REVIEW, new_reviews)

            self.bootler.api_call(
                "chat.postMessage",
                channel=self.review_channel,
                as_user=True,
                link_names=True,
                text=NEW_REVIEWS_MESSAGE.format(
                    len(new_reviews),
                    "s" if len(new_reviews) > 1 else "",
                    new_review_info
                )
            )

        needs_second_ids = []
        updated_ids = []
        rejected_ids = []
        for review_id, status in latest_reviews.items():
            if review_id not in self.last_known_reviews:
                continue

            last_known_review_status = self.last_known_reviews[review_id]

            if status != last_known_review_status:
                if status == "accepted":
                    needs_second_ids.append(review_id)
                elif status == "needs-revision":
                    rejected_ids.append(review_id)
                elif status == "needs-review":
                    updated_ids.append(review_id)

        updated_summary = ""
        if needs_second_ids:
            needs_second_reviews = self.phabricator.get_reviews(needs_second_ids)
            needs_second_reviews_info = (
                self.create_review_summary(ACCEPTED_REVIEW, needs_second_reviews)
            )

            updated_summary += NEEDS_SECOND_MESSAGE.format(
                len(needs_second_ids),
                "s" if len(needs_second_ids) != 1 else "",
                "" if len(needs_second_ids) != 1 else "s",
                needs_second_reviews_info
            )

        if updated_ids:
            updated_reviews = self.phabricator.get_reviews(updated_ids)
            updated_review_info = (
                self.create_review_summary(ACCEPTED_REVIEW, updated_reviews)
            )

            updated_summary += UPDATED_MESSAGE.format(
                len(updated_ids),
                "s" if len(updated_ids) != 1 else "",
                "ve" if len(updated_ids) != 1 else "s",
                updated_review_info
            )

        if rejected_ids:
            rejected_reviews = self.phabricator.get_reviews(rejected_ids)
            rejected_review_info = (
                self.create_review_summary(REVIEW, rejected_reviews)
            )

            updated_summary += REJECTED_MESSAGE.format(
                len(rejected_ids),
                "s" if len(rejected_ids) != 1 else "",
                "ve" if len(rejected_ids) != 1 else "s",
                rejected_review_info
            )

        if updated_summary:
            self.bootler.api_call(
                "chat.postMessage",
                channel=self.review_channel,
                as_user=True,
                link_names=True,
                text=UPDATED_REVIEWS_MESSAGE.format(updated_summary)
            )

        self.last_known_reviews = latest_reviews

    def check_for_build_updates(self):
        try:
            latest_pipeline_status = self.gocd.get_pipelines()
        except Exception as e:
            print "Failed to retrieve pipeline status.\n", str(e)

            return

        if self.last_pipeline_status is None:
            self.last_pipeline_status = latest_pipeline_status

            return

        attachments = []

        for group_name, group in latest_pipeline_status.items():
            if group_name not in self.last_pipeline_status:
                continue

            last_group_status = self.last_pipeline_status[group_name]
            for pipeline_name, pipeline in group.items():
                if pipeline_name not in last_group_status:
                    continue

                last_pipeline_status = last_group_status[pipeline_name]

                status = None

                currently_building = False
                previously_building = False
                for stage, info in last_pipeline_status["stages"].items():
                    result = info["result"]
                    if result == "unknown":
                        previously_building = True

                stage = None
                for stage, info in pipeline["stages"].items():
                    result = info["result"]
                    if stage not in last_pipeline_status["stages"]:
                        continue

                    last_result = last_pipeline_status["stages"][stage]["result"]

                    if last_result != result:
                        passed = (
                            result == "passed" and
                            (status is None or status == "passed") and
                            not currently_building
                        )
                        if passed:
                            status = "passed"
                        elif result == "unknown":
                            currently_building = True
                            if not previously_building:
                                status = "building"
                                stage = {"name": stage, "info": info}
                            else:
                                status = None
                            break
                        elif result == "N/A":
                            continue
                        elif result not in ["unknown", "passed"]:
                            stage = {"name": stage, "info": info}
                            status = "failed"
                            break

                if status == "passed":
                    message = PIPELINE_GOOD.format(pipeline_name)
                    attachments.append(
                        {
                            "text": message,
                            "fallback": message,
                            "color": "good"
                        }
                    )
                elif status == "building":
                    message = PIPELINE_BUILDING.format(
                        PIPELINE_STAGE_LINK.format(
                            pipeline=pipeline_name,
                            instance=pipeline["counter"],
                            stage=stage["name"],
                            counter=stage["info"]["counter"]
                        )
                        if stage is not None else
                        pipeline_name
                    )
                    attachments.append(
                        {
                            "text": message,
                            "fallback": message,
                            "color": "warning"
                        }
                    )
                elif status == "failed":
                    breaker = self.team.get_member_by_phabricator_name(
                        pipeline["changes"][0]["name"]
                    )
                    message = PIPELINE_BUSTED.format(
                        (
                            PIPELINE_STAGE_LINK.format(
                                pipeline=pipeline_name,
                                instance=pipeline["counter"],
                                stage=stage["name"],
                                counter=stage["info"]["counter"]
                            )
                            if stage is not None else
                            pipeline_name
                        ),
                        breaker.slack_name
                        if breaker is not None else
                        UNKNOWN_USER
                    )

                    attachments.append(
                        {
                            "text": message,
                            "fallback": message,
                            "color": "danger"
                        }
                    )

        if attachments:
            try:
                self.bootler.api_call(
                    "chat.postMessage",
                    channel=self.review_channel,
                    as_user=True,
                    link_names=True,
                    text=BUILD_STATUS_CHANGE_MESSAGE,
                    attachments=attachments
                )
            except Exception as e:
                print "Failed to send build update message - ", e

        self.last_pipeline_status = latest_pipeline_status

    def check_for_release_updates(self):
        releases_by_status, _ = self.jira.get_release_tickets()

        if releases_by_status is None:
            return

        releases = {
            raven_id: release
            for status, releases in releases_by_status.items()
            for raven_id, release in releases.items()
        }

        if self.last_release_status is None:
            self.last_release_status = releases

            return

        updated_releases={}

        for raven_id, release in releases.items():
            last_release_status = self.last_release_status.get(raven_id, None)

            new_status = release["status"]
            release_changed = (
                new_status in RELEVANT_RELEASE_STATUSES and
                last_release_status is not None and
                last_release_status["status"] != new_status
            )

            if not release_changed:
                continue

            if release["status"] not in updated_releases:
                updated_releases[new_status] = {}

            updated_releases[new_status][raven_id] = release

        if updated_releases:
            self.bootler.api_call(
                "chat.postMessage",
                channel=self.review_channel,
                as_user=True,
                link_names=True,
                text=RELEASE_UPDATES.format(
                    self.create_release_summary(updated_releases)
                )
            )

        self.last_release_status = releases

def parse_config():
    config = Config(
        os.path.basename(__file__),
        description=(
            "Sends slack notifications about reviews, builds, releases and "
            "more!"
        )
    )

    config.add_from_dictionary(
        {
            "verify_children": "false",
            "nodes": [
                {
                    "name": "teamName",
                    "default_value": "Savauge",
                    "description": "The team to which this butler belongs."
                },
                {
                    "name": "triageReferenceDate",
                    "default_value": "Mar 5, 2018",
                    "description": (
                        "The date from which the triage rotation starts."
                    )
                },
                {
                    "name": "standUp",
                    "description": "Stand Up message settings.",
                    "node_type": "group",
                    "nodes": [
                        {
                            "name": "message",
                            "default_value": "*_Time to Stand Up!_* {}",
                            "description": (
                                "The message to send to signal a daily stand up."
                            )
                        },
                        {
                            "name": "time",
                            "default_value": "11:50am",
                            "description": (
                                "The message to send to signal a daily stand up."
                            )
                        }
                    ]
                },
                {
                    "name": "log",
                    "description": "Log settings",
                    "node_type": "group",
                    "nodes": [
                        {
                            "name": "directory",
                            "default_value": (
                                "/oanda/var/bootler/logs"
                            ),
                            "description": (
                                "The directory in which bootler logs are "
                                "written."
                            )
                        },
                        {
                            "name": "filenameFormat",
                            "default_value": (
                                "bootler.{year}.{month:02}.{day:02}.log"
                            ),
                            "description": "The format of the log filename."
                        },
                        {
                            "name": "level",
                            "default_value": "info",
                            "description": (
                                "One of debug, info, warning, error, or "
                                "critical."
                            )
                        }
                    ]
                },
                {
                    "name": "phabricator",
                    "description": "phabricator server settings.",
                    "node_type": "group",
                    "nodes": [
                        {
                            "name": "base_url",
                            "default_value": (
                                "https://phabricator.dev.oanda.com/"
                            ),
                            "description": "The phabricator base url."
                        },
                        {
                            "name": "token",
                            "default_value": "",
                            "description": "The phabricator api token."
                        },
                        {
                            "name": "queryKey",
                            "default_value": "",
                            "description": (
                                "The phabricator key used to query revisions."
                            )
                        }
                    ]
                },
                {
                    "name": "jira",
                    "description": "JIRA server settings.",
                    "node_type": "group",
                    "nodes": [
                        {
                            "name": "user",
                            "default_value": "bootler@oanda.com",
                            "description": "The JIRA user used to access the API."
                        },
                        {
                            "name": "token",
                            "default_value": "",
                            "description": (
                                "The token used to access the JIRA API."
                            )
                        },
                        {
                            "name": "triageQuery",
                            "default_value": "",
                            "description": (
                                "The JQL statement used to retrieve triage "
                                "tickets."
                            )
                        },
                        {
                            "name": "releaseQuery",
                            "default_value": "",
                            "description": (
                                "The JQL statement used to retrieve release "
                                "tickets."
                            )
                        }
                    ]
                },
                {
                    "name": "confluence",
                    "description": "Confluence server settings.",
                    "node_type": "group",
                    "nodes": [
                        {
                            "name": "user",
                            "default_value": "bootler@oanda.com",
                            "description": "The JIRA user used to access the API."
                        },
                        {
                            "name": "token",
                            "default_value": "",
                            "description": (
                                "The token used to access the JIRA API."
                            )
                        },
                        {
                            "name": "triageSchedulePageID",
                            "default_value": "",
                            "description": (
                                "The ID of the triage schedule page."
                            )
                        },
                        {
                            "name": "contentMarker",
                            "default_value": (
                                "<ac:layout-cell><h4>"
                                "<strong>Backend Execution "
                                "Triage Schedule</strong></h4>"
                            ),
                            "description": (
                                "The test that marks the start of the "
                                "html table to modify."
                            )
                        },
                        {
                            "name": "tableTemplateFile",
                            "default_value": "",
                            "description": (
                                "The file containing the triage schedule "
                                "table template."
                            )
                        }
                    ]
                },
                {
                    "name": "gocd",
                    "description": "gocd server settings.",
                    "node_type": "group",
                    "nodes": [
                        {
                            "name": "username",
                            "default_value": "qauser",
                            "description": "The gocd api username."
                        },
                        {
                            "name": "password",
                            "default_value": "",
                            "description": (
                                "The gocd api password for the given username."
                            )
                        },
                        {
                            "name": "groups",
                            "default_value": "",
                            "description": (
                                "The gocd pipeline groups to observe."
                            )
                        }
                    ]
                },
                {
                    "name": "slack",
                    "description": "slack server settings.",
                    "node_type": "group",
                    "nodes": [
                        {
                            "name": "token",
                            "default_value": "",
                            "description": "The slack api token."
                        },
                        {
                            "name": "triageChannel",
                            "default_value": "bootler-test",
                            "description": (
                                "The channel to which triage notifications will be sent."
                            )
                        },
                        {
                            "name": "reviewChannel",
                            "default_value": "bootler-test",
                            "description": (
                                "The channel to which review notifications will be sent."
                            )
                        }
                    ]
                },
                {
                    "name": "team",
                    "description": "The list of team members.",
                    "node_type": "group",
                    "allow_multiple": True,
                    "nodes": [
                        {
                            "name": "firstName",
                            "default_value": "",
                            "description": "The first name of the team member."
                        },
                        {
                            "name": "lastName",
                            "default_value": "",
                            "description": "The last name of the team member."
                        },
                        {
                            "name": "phabricatorName",
                            "default_value": "",
                            "description": (
                                "The name of the team member in phabricator."
                            )
                        },
                        {
                            "name": "slackName",
                            "default_value": "",
                            "description": (
                                "The name of the team member in slack."
                            )
                        },
                        {
                            "name": "gender",
                            "default_value": "",
                            "description": "The gender of the team member."
                        },
                        {
                            "name": "excludeFromTriage",
                            "data_type": "bool",
                            "default_value": False,
                            "description": (
                                "Whether to exclude the member from the "
                                "triage rotation."
                            )
                        }
                    ]
                }
            ]
        }
    )

    if not config.parse():
        config.dump()

        return False

    return config

def main():
    config = parse_config()

    print(config.phabricator)

    bootler = Bootler(config)

    running = True

    # bootler.daily_summary()
    # bootler.daily_post_lunch_summary()
    # bootler.daily_stand_up()
    # print bootler.team.get_triage_schedule()
    # bootler.update_triage_schedule()
    # bootler.triage_person_change()

    previous = datetime.now()
    while running:
        now = datetime.now()

        bootler.serve(previous, now)

        previous = now

        print "Bootler is taking a 15 second break."
        sleep(15)

# main()

if __name__ == '__main__':
    main()