import calendar
import json

from functools import partial
from time import sleep
from datetime import *
from dateutil.parser import parse
from random import randint
from slackclient import SlackClient

import toornament

from schedule import Schedule

vs = [
    "hosts",
    "faces",
    "takes on",
    "plays"
]

DAILY_MESSAGE = (
    "<!here> *PPCSC Day {}* Today's Schedule:\n"
    "{}\n"
    "*_The road to the JERRY Cup Continues!_*"
)
NEXT_DAY_MESSAGE = (
    "<!here> {}'s (*{}*) Schedule:\n"
    "{}\n"
    "*_The road to the JERRY Cup Continues!_*"
)
DAILY_MATCH_MESSAGE = "*{}* - {} {} {}"
MATCH_REMINDER_MESSAGE = (
    "<!here> *{}* vs *{}* starts in 10 minutes! If you're one of the "
    "people listed, proceed to the Ping Pong table."
)
STREAM_REMINDER_MESSAGE = (
    "<!here> *{}* vs *{}* is starting! <https://www.twitch.tv/oanda_rlcsc|Watch here!>"
)

HOME_GAME_UPDATE = "*_{} Scores!_* (*{} {}* - {} {})"
AWAY_GAME_UPDATE = "*_{} Scores!_* ({} {} - *{} {}*)"
HOME_MATCH_UPDATE = "*_{} takes game {}!_* (Match *{} {}* - {} {})"
AWAY_MATCH_UPDATE = "*_{} takes game {}!_* (Match {} {} - *{} {}*)"
HOME_MATCH_COMPLETE = "*_{} wins the match!_* (Match *{} {}* - {} {})"
AWAY_MATCH_COMPLETE = "*_{} wins the match!_* (Match {} {} - *{} {}*)"

# CHANNEL = "rlcsc"
CHANNEL = "gerwin-test"

class Gerwin(object):
    def __init__(self):
        self.slack_client = SlackClient(
            "xoxb-356598396085-rr78x88sDRtljZELHJQ9Kttb"
        )

        if not self.slack_client.rtm_connect(with_team_state=False):
            print "Gerwin did not materialize."

            return False

        gerwin_id = self.slack_client.api_call("auth.test")["user_id"]

        self.toornament = toornament.Client(
            "1353471592693735424",
            "0YcHPAFXc6YUO5rlFPFueyauKgQaW_ipXlym9KIemGg"
        )

        self.message_schedule = Schedule([])

        self.process_daily_schedule()

        self.refresh_event_schedule()

        self.current_match_data = None

    def serve(self, start, end):
        self.message_schedule.trigger_events(start, end)

    def refresh_event_schedule(self):
        events = [
            {
                "type": "Daily",
                "include_weekends": False,
                "time": "9:45am",
                "event": self.send_daily_schedule
            },
            {
                "type": "Daily",
                "include_weekends": False,
                "time": "3:30pm",
                "event": self.send_next_day_schedule
            },
            {
                "type": "Repeated",
                "frequency": 15,
                "event": self.check_for_match_updates
            },
        ]

        for date, matches in self.daily_schedule.items():
            for gametime, match in matches.items():
                if gametime < datetime.now():
                    continue

                events.append(
                    {
                        "type": "Relative",
                        "datetime": str(gametime),
                        "delta": 30,
                        "event": partial(
                            self.send_stream_reminder,
                            match["home"],
                            match["away"]
                        )
                    }
                )
                events.append(
                    {
                        "type": "Relative",
                        "datetime": str(gametime),
                        "delta": 600,
                        "event": partial(
                            self.send_match_reminder,
                            match["home"],
                            match["away"]
                        )
                    }
                )

        self.message_schedule.refresh(events)

    def process_daily_schedule(self):
        self.matches = self.toornament.get_matches()

        today = date.today()

        self.daily_schedule = {}
        for match in self.matches:
            gametime = match["gametime"]
            if gametime is None:
                continue

            match_date = gametime.date()

            if match_date < today:
                continue

            if match_date not in self.daily_schedule:
                self.daily_schedule[match_date] = {}

            self.daily_schedule[match_date][gametime] = {
                "id": match["id"],
                "home": match["home"],
                "away": match["away"]
            }

    def determine_day_count(self, today):
        days = []
        for match in self.matches:
            gametime = match["gametime"]
            if gametime is None:
                continue

            day = gametime.date()
            if day not in days:
                days.append(day)

        days.sort()

        day_count = 0
        for day in days:
            if day > today:
                break

            day_count += 1

        return day_count

    def send_daily_schedule(self):
        today = date.today()

        matches = []
        if today in self.daily_schedule:
            sorted_game_times = [
                gametime for gametime in self.daily_schedule[today].keys()
            ]
            sorted_game_times.sort()

            for game_time in sorted_game_times:
                teams = self.daily_schedule[today][game_time]
                matches.append(
                    DAILY_MATCH_MESSAGE.format(
                        game_time.strftime("%I:%M%p"),
                        teams["home"],
                        vs[randint(0, len(vs) - 1)],
                        teams["away"],
                    )
                )

        self.slack_client.api_call(
            "chat.postMessage",
            channel=CHANNEL,
            as_user=True,
            link_names=True,
            text=(
                DAILY_MESSAGE.format(
                    self.determine_day_count(date.today()),
                    "\n".join(matches)
                )
                if matches else
                "*RLCSCPA Mandated Rest Day* - No matches scheduled."
            )
        )

    def send_next_day_schedule(self):
        today = date.today()

        sorted_dates = [
            gamedate for gamedate in self.daily_schedule.keys()
        ]
        sorted_dates.sort()

        next_date = None
        for gamedate in sorted_dates:
            if today < gamedate:
                next_date = gamedate
                break

        matches = []
        if next_date in self.daily_schedule:
            sorted_game_times = [
                gametime for gametime in self.daily_schedule[next_date].keys()
            ]
            sorted_game_times.sort()

            for game_time in sorted_game_times:
                teams = self.daily_schedule[next_date][game_time]
                matches.append(
                    DAILY_MATCH_MESSAGE.format(
                        game_time.strftime("%I:%M%p"),
                        teams["home"],
                        vs[randint(0, len(vs) - 1)],
                        teams["away"]
                    )
                )

        self.slack_client.api_call(
            "chat.postMessage",
            channel=CHANNEL,
            as_user=True,
            link_names=True,
            text=(
                NEXT_DAY_MESSAGE.format(
                    calendar.day_name[next_date.weekday()],
                    next_date.strftime("%B %d").replace(" 0"," "),
                    "\n".join(matches)
                )
                if matches else
                "*PPCSCPA is Done! See you next time?*"
            )
        )

    def send_match_reminder(self, home, away):
        self.slack_client.api_call(
            "chat.postMessage",
            channel=CHANNEL,
            as_user=True,
            link_names=True,
            text=(MATCH_REMINDER_MESSAGE.format(home, away))
        )

    def send_stream_reminder(self, home, away):
        self.slack_client.api_call(
            "chat.postMessage",
            channel=CHANNEL,
            as_user=True,
            link_names=True,
            text=STREAM_REMINDER_MESSAGE.format(home, away)
        )

    def update_matches(self):
        pass

    def check_for_match_updates(self):
        today = date.today()
        if today not in self.daily_schedule:
            print "No matches to check today."

            return

        match_ids = [
            match["id"] for match in self.daily_schedule[today].values()
        ]

        new_match_data = self.toornament.get_match_data(match_ids)

        if self.current_match_data is None:
            self.current_match_data = new_match_data

            return

        print json.dumps(
            self.current_match_data,
            indent=4
        )
        print json.dumps(
            new_match_data,
            indent=4
        )

        for match_id, match in new_match_data.items():
            if match_id not in self.current_match_data:
                print "New match found {}".format(match_id)

                self.current_match_data[match_id] = match

                continue

            old_match_state = self.current_match_data[match_id]
            new_match_state = new_match_data[match_id]

            home = new_match_state["home"]
            away = new_match_state["away"]

            #
            # Game updates
            #
            old_games_state = old_match_state["games"]
            new_games_state = new_match_state["games"]
            for game in range(0, len(new_games_state)):
                old_home_game_score = old_games_state[game]["home"]
                new_home_game_score = new_games_state[game]["home"]

                old_away_game_score = old_games_state[game]["away"]
                new_away_game_score = new_games_state[game]["away"]
                home_game_score_update = (
                    old_home_game_score < new_home_game_score
                )
                if home_game_score_update:
                    self.slack_client.api_call(
                        "chat.postMessage",
                        channel=CHANNEL,
                        as_user=True,
                        link_names=True,
                        text=HOME_GAME_UPDATE.format(
                            home,
                            home,
                            new_home_game_score,
                            new_away_game_score,
                            away
                        )
                    )

                    old_match_state["games"][game]["home"] = new_home_game_score

                away_game_score_update = (
                    old_away_game_score < new_away_game_score
                )
                if away_game_score_update:
                    self.slack_client.api_call(
                        "chat.postMessage",
                        channel=CHANNEL,
                        as_user=True,
                        link_names=True,
                        text=AWAY_GAME_UPDATE.format(
                            away,
                            home,
                            new_home_game_score,
                            new_away_game_score,
                            away
                        )
                    )

                    old_match_state["games"][game]["away"] = new_away_game_score

            #
            # Match updates
            #
            old_home_match_score = old_match_state["score"]["home"]
            new_home_match_score = new_match_state["score"]["home"]

            old_away_match_score = old_match_state["score"]["away"]
            new_away_match_score = new_match_state["score"]["away"]

            home_match_score_update = (
                old_home_match_score < new_home_match_score
            )
            if home_match_score_update:
                self.slack_client.api_call(
                    "chat.postMessage",
                    channel=CHANNEL,
                    as_user=True,
                    link_names=True,
                    text=HOME_MATCH_UPDATE.format(
                        home,
                        (new_home_match_score + new_away_match_score),
                        home,
                        new_home_match_score,
                        new_away_match_score,
                        away
                    )
                )

                old_match_state["score"]["home"] = new_home_match_score

            away_match_score_update = (
                old_away_match_score < new_away_match_score
            )
            if away_match_score_update:
                self.slack_client.api_call(
                    "chat.postMessage",
                    channel=CHANNEL,
                    as_user=True,
                    link_names=True,
                    text=AWAY_MATCH_UPDATE.format(
                        away,
                        (new_home_match_score + new_away_match_score),
                        home,
                        new_home_match_score,
                        new_away_match_score,
                        away
                    )
                )

                old_match_state["score"]["away"] = new_away_match_score

            old_match_status = old_match_state["status"]
            new_match_status = new_match_state["status"]

            match_completed = (
                old_match_status != new_match_status and
                new_match_status == "completed"
            )
            if match_completed:
                if new_home_match_score > new_away_match_score:
                    self.slack_client.api_call(
                        "chat.postMessage",
                        channel=CHANNEL,
                        as_user=True,
                        link_names=True,
                        text=HOME_MATCH_COMPLETE.format(
                            home,
                            home,
                            new_home_match_score,
                            new_away_match_score,
                            away
                        )
                    )
                else:
                    self.slack_client.api_call(
                        "chat.postMessage",
                        channel=CHANNEL,
                        as_user=True,
                        link_names=True,
                        text=AWAY_MATCH_COMPLETE.format(
                            away,
                            home,
                            new_home_match_score,
                            new_away_match_score,
                            away
                        )
                    )

                old_match_state["status"] = new_match_state["status"]


def main():
    running = True

    gerwin = Gerwin()

    previous = datetime.now()
    while running:
        now = datetime.now()

        gerwin.serve(previous, now)

        previous = now

        print "Gerwin is going back to sleep."
        sleep(15)

main()
