import json
import requests
import base64
import pytz

from sets import Set
from time import sleep
from datetime import *
from heapq import heappush, heappop
from dateutil.parser import parse
from pytz import timezone

from slackclient import SlackClient

import phabricator
from schedule import Schedule
from team import Team

class Client(object):
    def __init__(self, tournament_id, api_key):
        self.tournament_id = tournament_id
        self.api_key = api_key

    def get_matches(self):
        result = json.loads(
            requests.get(
                "https://api.toornament.com/viewer/v2/"
                "tournaments/{tournament_id}/matches".format(
                    tournament_id=self.tournament_id
                ),
                headers={
                    "X-Api-Key": self.api_key,
                    "Range": "matches=0-127"
                },
                data={
                    "scheduled_after": pytz.utc.localize(
                        datetime.now(),
                        is_dst=None
                    ).astimezone(pytz.timezone('America/Toronto')).isoformat(),
                    "sort": "schedule"
                }
            ).text
        )

        eastern = timezone("US/Eastern")

        matches = [
            {
                "id": match["id"],
                "status": match["status"],
                "gametime": (
                    pytz.utc.localize(
                        parse(
                            match[
                                "scheduled_datetime"
                            ][
                                :match["scheduled_datetime"].find("+")
                            ]
                        )
                    ).astimezone(eastern).replace(tzinfo=None)
                    if match["scheduled_datetime"] is not None else
                    None
                ),
                "home": (
                    match["opponents"][0]["participant"].get("name", "TBD")
                    if match["opponents"][0]["participant"] is not None else
                    "TBD"
                ),
                "away": (
                    match["opponents"][1]["participant"].get("name", "TBD")
                    if match["opponents"][1]["participant"] is not None else
                    "TBD"
                )
            }
            for match in result
        ]

        return matches

    def get_match_data(self, match_ids):
        match_data = {}
        for match_id in match_ids:
            result = json.loads(
                requests.get(
                    "https://api.toornament.com/viewer/v2/"
                    "tournaments/{tournament_id}/matches/{match_id}".format(
                        tournament_id=self.tournament_id,
                        match_id=match_id
                    ),
                    headers={
                        "X-Api-Key": self.api_key
                    }
                ).text
            )

            match_data[match_id] = {
                "status": result["status"],
                "home": (
                    result["opponents"][0]["participant"].get("name", "TBD")
                    if result["opponents"][0]["participant"] is not None else
                    "TBD"
                ),
                "away": (
                    result["opponents"][1]["participant"].get("name", "TBD")
                    if result["opponents"][1]["participant"] is not None else
                    "TBD"
                ),
                "games" : [
                    {
                        "status": game["status"],
                        "home": (
                            game["opponents"][0]["score"]
                            if game["opponents"][0]["score"] is not None else
                            0
                        ),
                        "away": (
                            game["opponents"][1]["score"]
                            if game["opponents"][1]["score"] is not None else
                            0
                        )
                    }
                    for game in result["games"]
                ],
                "score": {
                    "home": (
                        result["opponents"][0]["score"]
                        if result["opponents"][0]["score"] is not None else
                        0
                    ),
                    "away": (
                        result["opponents"][1]["score"]
                        if result["opponents"][1]["score"] is not None else
                        0
                    )
                }
            }

        return match_data
