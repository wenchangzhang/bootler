import json
import requests

from sets import Set
from time import sleep
from datetime import *
from heapq import heappush, heappop
from dateutil.parser import parse

class Schedule(object):
    days_of_the_week = {
        "monday": 0,
        "tuesday": 1,
        "wednesday": 2,
        "thursday": 3,
        "friday": 4,
        "saturday": 5,
        "sunday": 6
    }

    class Base(object):
        def __init__(self, event_handler):
            self.event_handler = event_handler

        def trigger_event(self):
            self.event_handler()

        def repeated(self):
            return False

        def __lt__(self, other):
            return self.trigger_datetime() < other.trigger_datetime()

        def __le__(self, other):
            return self.trigger_datetime() <= other.trigger_datetime()

    class Repeated(Base):
        def __init__(self, frequency, event_handler):
            super(Schedule.Repeated, self).__init__(event_handler)

            self.frequency = frequency

            self.trigger_time = datetime.now() + timedelta(0, self.frequency)

        def trigger_datetime(self):
            return self.trigger_time

        def repeated(self):
            return True

        def next_event(self):
            return Schedule.Repeated(self.frequency, self.event_handler)

    class Daily(Base):
        def __init__(self, time, include_weekends, event_handler):
            super(Schedule.Daily, self).__init__(event_handler)

            self.time = time
            self.include_weekends = include_weekends

            event_date = date.today()
            now_time = datetime.now().time()
            if now_time >= self.time:
                if not self.include_weekends and event_date.weekday() >= 4:
                    event_date += timedelta(6 - event_date.weekday() + 1)
                else:
                    event_date += timedelta(1)

            self.trigger_time = datetime.combine(event_date, self.time)

            print "Next Daily: ", self.trigger_time

        def trigger_datetime(self):
            return self.trigger_time

        def repeated(self):
            return True

        def next_event(self):
            return Schedule.Daily(
                self.time,
                self.include_weekends,
                self.event_handler
            )

    class Weekly(Base):
        def __init__(self, time, day_of_the_week, event_handler):
            super(Schedule.Weekly, self).__init__(event_handler)

            self.day_of_the_week = day_of_the_week
            self.time = time

            now = datetime.combine(date.today(), time)

            self.trigger_time = (
                now + timedelta(self.day_of_the_week - now.weekday())
                if self.day_of_the_week > now.weekday() or (
                    self.day_of_the_week == now.weekday() and
                    self.time > datetime.now().time()
                ) else
                now + timedelta(
                    len(Schedule.days_of_the_week) -
                    (now.weekday() - self.day_of_the_week)
                )
            )

            print "New Trigger Time is {}".format(self.trigger_time)

        def trigger_datetime(self):
            return self.trigger_time

        def repeated(self):
            return True

        def next_event(self):
            return Schedule.Weekly(
                self.time,
                self.day_of_the_week,
                self.event_handler
            )

    class Relative(Base):
        def __init__(self, datetime, time_delta, event_handler):
            super(Schedule.Relative, self).__init__(event_handler)

            self.trigger_time = datetime - time_delta

            print str(self.trigger_time)


        def trigger_datetime(self):
            return self.trigger_time

    def __init__(self, events):
        self.events = []
        self.add_events(events)

    def refresh(self, events):
        self.events = []

        self.add_events(events)

    def add_events(self, events):
        for event in events:
            event_type = event['type'].lower()
            if event_type == Schedule.Daily.__name__.lower():
                heappush(
                    self.events,
                    Schedule.Daily(
                        parse(event['time']).time(),
                        event['include_weekends'],
                        event['event']
                    )
                )
            elif event_type == Schedule.Weekly.__name__.lower():
                heappush(
                    self.events,
                    Schedule.Weekly(
                        parse(event['time']).time(),
                        Schedule.days_of_the_week[event['day of the week']],
                        event['event']
                    )
                )
            elif event_type == Schedule.Relative.__name__.lower():
                heappush(
                    self.events,
                    Schedule.Relative(
                        parse(event['datetime']),
                        timedelta(0, event['delta']),
                        event['event']
                    )
                )
            elif event_type == Schedule.Repeated.__name__.lower():
                heappush(
                    self.events,
                    Schedule.Repeated(
                        event['frequency'],
                        event['event']
                    )
                )

    def trigger_events(self, range_start, range_end):
        next_event = self.events[0]

        print "Checking {} < {} < {}".format(
            range_start,
            next_event.trigger_datetime(),
            range_end
        )

        while next_event.trigger_datetime() <= range_end:
            print "Triggering {} (< {} is {})".format(
                next_event.trigger_datetime(),
                range_end,
                next_event.trigger_datetime() <= range_end
            )
            triggered_event = heappop(self.events)
            triggered_event.trigger_event()

            if triggered_event.repeated():
                heappush(self.events, triggered_event.next_event())

            next_event = self.events[0]
