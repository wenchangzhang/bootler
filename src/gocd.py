import json
import requests

from sets import Set
from time import sleep
from datetime import *
from heapq import heappush, heappop
from dateutil.parser import parse

class Client(object):
    def __init__(self, username, password, groups):
        self.username = username
        self.password = password
        self.groups = groups

    def parse_username_from_gocd_name(self, gocd_name):
        if gocd_name is None or "@" not in gocd_name or "<" not in gocd_name:
            return "???"

        return gocd_name[gocd_name.find('<') + 1:gocd_name.find('@')]

    def get_pipeline_status(self, link):
        response = requests.get(
            link,
            auth=(self.username, self.password)
        ).text

        try:
            result = json.loads(response)
        except:
            print "Failed to parse gocd response", response
            raise

        # print json.dumps(result, indent=4)

        return (
            result["name"],
            {
                "changes": [
                    {
                        "name": self.parse_username_from_gocd_name(
                            modification["user_name"]
                        ),
                        "comment": modification["comment"]
                    }
                    for modification in
                    result["build_cause"]["material_revisions"][0]["modifications"]
                ],
                "stages": {
                    stage["name"]: {
                        "result": (
                            stage["result"].lower()
                            if "result" in stage else
                            "N/A"
                        ),
                        "counter": stage["counter"]
                    }
                    for stage in result["stages"]
                },
                "counter": result["counter"]
            }
        )


    def get_pipelines(self):
        result = json.loads(
            requests.get(
                "https://gocd-master.svc.ri.oanda.com/go/api/dashboard",
                # "https://gocd-master-uat.svc.ri.oanda.com/go/api/dashboard",
                auth=(self.username, self.password),
                headers={"Accept": "application/vnd.go.cd.v2+json"}
            ).text
        )

        pipeline_groups = [
            {
                "name": group["name"],
                "pipelines": [
                    {
                        "name": pipeline["name"],
                        "link": (
                            pipeline["_embedded"]["instances"][0]["_links"]["self"]
                        )
                    }
                    for pipeline in result["_embedded"]["pipelines"]
                    if pipeline["_embedded"]["instances"] and
                    pipeline["name"] in group["pipelines"]

                ]
            }
            for group in result["_embedded"]["pipeline_groups"]
            if group["name"] in self.groups
        ]

        print(json.dumps(pipeline_groups, indent=4))

        return {
            group["name"]: dict(
                [
                    self.get_pipeline_status(pipeline["link"]["href"])
                    for pipeline in group["pipelines"]
                ]
            )
            for group in pipeline_groups
        }

