import json
import requests

from sets import Set
from time import sleep
from datetime import *
from heapq import heappush, heappop
from dateutil.parser import parse

class Client(object):
    def __init__(self, token, query_key):
        self.token = token
        self.query_key = query_key

    def get_revision_data(self):
        print(self.token, self.query_key)

        try:
            result = json.loads(
                requests.get(
                    "https://phabricator.dev.oanda.com/api/differential.revision.search",
                    data={
                        "api.token": self.token,
                        "queryKey": self.query_key,
                        "attachments[reviewers]": True
                    }
                ).text
            )
        except:
            print "Failed to reach phabricator."

            return None

        if "result" not in result:
            print "Phabricator call failed:\n{}".format(str(result))

            return None

        if result["result"] is None:
            print "Phabricator call failed:\n{}".format(str(result))

            return None

        if "data" not in result["result"]:
            print "Phabricator call failed:\n{}".format(str(result))

            return None

        return result["result"]["data"]

    def query_revision_ids(self):
        data = self.get_revision_data()
        if data is None:
            return None

        return {
            review["id"]: review["fields"]["status"]["value"]
            for review in data
        }

    def get_reviews(self, new_review_ids):
        data = self.get_revision_data()

        return [
            {
                "id": review["id"],
                "title": review["fields"]["title"],
                "author": self.get_user_name(
                    review["fields"]["authorPHID"]
                ),
                "repo": self.get_repo_name(
                    review["fields"]["repositoryPHID"]
                ),
                "reviewers": self.get_reviewers(review),
                "size": self.get_size(review["fields"]["diffPHID"])
            }
            for review in data
            if review["id"] in new_review_ids
        ]

    def query_revisions(self):
        data = self.get_revision_data()
        for review in data:
            print review["id"], review["fields"]["status"]["value"]

        return {
            "Needs Review": [
                {
                    "id": review["id"],
                    "title": review["fields"]["title"],
                    "author": self.get_user_name(
                        review["fields"]["authorPHID"]
                    ),
                    "repo": self.get_repo_name(
                        review["fields"]["repositoryPHID"]
                    ),
                    "reviewers": [],
                    "size": self.get_size(review["fields"]["diffPHID"])
                }
                for review in data
                if review["fields"]["status"]["value"] == "needs-review"
            ],
            "Accepted": [
                {
                    "id": review["id"],
                    "title": review["fields"]["title"],
                    "author": self.get_user_name(
                        review["fields"]["authorPHID"]
                    ),
                    "repo": self.get_repo_name(
                        review["fields"]["repositoryPHID"]
                    ),
                    "reviewers": self.get_reviewers(review),
                    "size": self.get_size(review["fields"]["diffPHID"])
                }
                for review in data
                if review["fields"]["status"]["value"] == "accepted"
            ],
            "Rejected": [
                {
                    "id": review["id"],
                    "title": review["fields"]["title"],
                    "author": self.get_user_name(
                        review["fields"]["authorPHID"]
                    ),
                    "repo": self.get_repo_name(
                        review["fields"]["repositoryPHID"]
                    ),
                    "reviewers": [],
                    "size": self.get_size(review["fields"]["diffPHID"])
                }
                for review in data
                if review["fields"]["status"]["value"] == "needs-revision"
            ]
        }

    def get_user_name(self, user_phid):
        result = json.loads(
            requests.get(
                "https://phabricator.dev.oanda.com/api/phid.query",
                data={
                    "api.token": self.token,
                    "phids[0]": user_phid
                }
            ).text
        )

        if "result" not in result:
            return "???"

        result_data = result["result"]

        if user_phid not in result_data:
            print "User phid not found in result"
            return "???"

        user = result_data[user_phid]
        if "name" not in user:
            print "name not found in field map"
            return "???"

        return user["name"]

    def get_repo_name(self, repo_phid):
        if repo_phid is None:
            return "???"

        result = json.loads(
            requests.get(
                "https://phabricator.dev.oanda.com/api/phid.query",
                data={
                    "api.token": self.token,
                    "phids[0]": repo_phid
                }
            ).text
        )

        if "result" not in result:
            return "???"

        result_data = result["result"]

        if repo_phid not in result_data:
            print "User phid not found in result"
            return "???"

        repo = result_data[repo_phid]
        if "name" not in repo:
            print "name not found in field map"
            return "???"

        return repo["fullName"][repo["fullName"].find(' ') + 1:]

    def get_reviewers(self, revision):
        if "attachments" not in revision:
            return []

        attachments = revision["attachments"]
        if "reviewers" not in attachments:
            return []

        reviewers_attachement = attachments["reviewers"]
        if "reviewers" not in reviewers_attachement:
            return []

        reviewers = reviewers_attachement["reviewers"]

        return [
            self.get_user_name(reviewer["reviewerPHID"])
            for reviewer in reviewers
            if "USER" in reviewer["reviewerPHID"]
        ]

    def get_diff_id(self, diff_phid):
        result = json.loads(
            requests.get(
                "https://phabricator.dev.oanda.com/api/differential.diff.search",
                data={
                    "api.token": self.token,
                    "constraints[phids][0]": diff_phid
                }
            ).text
        )

        if "result" not in result:
            return None

        result_data = result["result"]

        if "data" not in result_data:
            print "User phid not found in result"
            return None

        diffs = result_data["data"]

        if len(diffs) != 1:
            print "Found {} diffs, should have only found 1.".format(len(diffs))
            return None

        diff = diffs[0]

        if "id" not in diff:
            print "id not found in diff"
            return None

        return diff["id"]

    def get_size(self, diff_phid):
        diff_id = self.get_diff_id(diff_phid)

        result_text = requests.get(
            "https://phabricator.dev.oanda.com/api/differential.getrawdiff",
            data={
                "api.token": self.token,
                "diffID": diff_id
            }
        ).text
        try:
            result = json.loads(result_text)
        except Exception as e:
            print e
            return "unknown"

        if "result" not in result:
            return "unknown"

        result_data = result["result"]

        line_count = len(result["result"].split())

        if line_count < 10:
            return "minuscule"
        elif line_count < 50:
            return "tiny"
        elif line_count < 200:
            return "small"
        elif line_count < 2000:
            return "medium"
        elif line_count < 20000:
            return "large"
        elif line_count < 100000:
            return "massive"

        return "outrageous"
