# ----------------------------------------------------------------------
# setup.py -- bootler setup script
#
# Copyright (C) 2018, OANDA Corporation.
# ----------------------------------------------------------------------

import os
import build_settings

from setuptools import setup, find_packages

# Run the setup
setup(
    name="bootler",
    version=build_settings.__build_version__,
    description="Bootler",
    author="OANDA Corporation",
    author_email="backend-dev@oanda.com",
    setup_requires=[],
    install_requires=[
        'af-python',
        'python-dateutil',
        'requests',
        'slackclient',
        'google-api-python-client'
    ],
    packages=find_packages(exclude='tests'),
    py_modules=[
        'bootler',
        'gocd',
        'googlecalendar',
        'phabricator',
        'team',
        'schedule',
        'confluence',
        'jira'
    ],
    entry_points={'console_scripts': ['bootler = bootler:main']},
    zip_safe=True,
    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'Environment :: Console',
        'Intended Audience :: Developers',
        'License :: Other/Proprietary License',
        'Natural Language :: English',
        'Operating System :: OS Independent',
        'Topic :: Software Development',
        'Topic :: Utilities'
    ]
)
