import json
import requests
import base64

from sets import Set
from time import sleep
from datetime import *
from heapq import heappush, heappop
from dateutil.parser import parse
from requests.auth import HTTPBasicAuth

class Client(object):
    def __init__(
        self,
        username,
        api_token,
        triage_page_id,
        content_marker,
        table_template_file
    ):
        self.username = username
        self.api_token = api_token
        self.triage_page_id = triage_page_id
        self.content_marker = content_marker
        self.content_end_marker = "</ac:layout-cell>"
        self.template = None

        try:
            with open(table_template_file, 'r') as template_file:
                lines = template_file.readlines()
                lines = [line.strip() for line in lines]
                self.template = "".join(lines)
        except Exception as e:
            print "Failed to read triage table template file.", str(e)

    def update_triage_schedule(self, team, new_schedule):
        response = requests.get(
            (
                "https://oandacorp.atlassian.net/wiki/"
                "rest/api/content/{}".format(self.triage_page_id)
            ),
            headers={
                'Authorization': 'Basic {}'.format(
                    base64.b64encode(
                        '{}:{}'.format(self.username, self.api_token)
                    )
                ),
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            params={"expand": "body.storage,version"}
        )

        result = None
        try:
            result = json.loads(response.text)
        except:
            print "Failed to parse Confluence page response:"
            print response

            return

        if "version" not in result:
            print "No version found in confluence response."

            return

        version = result["version"]

        if "number" not in version:
            print "No version number found in confluence response."

            return

        if "body" not in result:
            print "No body found in confluence response."

            return

        body = result["body"]

        if "storage" not in body:
            print "No storage found in confluence page body response."

            return

        storage = body["storage"]

        if "value" not in storage:
            print "No content found in confluence page body response."

            return

        content = storage["value"]

        print content

        if self.content_marker not in content:
            print "Could not find start of content to modify."

            return

        content_start_index = content.find(self.content_marker)
        content_end_index = (
            content.find(self.content_end_marker, content_start_index) +
            len(self.content_end_marker)
        )

        pre_schedule = content[:content_start_index]

        post_schedule = content[content_end_index:]

        template_values = {}

        for week in range(1, len(new_schedule) + 1):
            triage_person = new_schedule[week - 1]
            week_start = triage_person["week_start"]
            week_end = week_start + timedelta(4)

            template_values["week{}".format(week)] = "{start} - {end}".format(
                start=week_start.strftime("%B %d").replace(" 0", " "),
                end=week_end.strftime("%B %d").replace(" 0", " ")
            )
            template_values["week{}member".format(week)] = self.get_user_key(
                triage_person["username"]
            )

        template_values["team"] = team

        updated_schedule = self.template.format(**template_values)

        new_content = "{}{}{}".format(
            pre_schedule,
            updated_schedule,
            post_schedule
        )

        response = requests.put(
            (
                "https://oandacorp.atlassian.net/wiki/"
                "rest/api/content/{}".format(self.triage_page_id)
            ),
            headers={
                'Authorization': 'Basic {}'.format(
                    base64.b64encode(
                        '{}:{}'.format(self.username, self.api_token)
                    )
                ),
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            data=json.dumps(
                {
                    "version": {"number": version["number"] + 1},
                    "type": result["type"],
                    "title": result["title"],
                    "body": {
                        "storage": {
                            "value": new_content,
                            "representation": "storage"
                        }
                    }
                }
            )
        )

        if response.status_code != 200:
            if response.status_code == 409:
                print (
                    "Failed to update version {} of the "
                    "Triage Schedule.".format(version["number"])
                )

            print response

    def get_user_key(self, username):
        response = requests.get(
            "https://oandacorp.atlassian.net/wiki/rest/api/user/",
            headers={
                'Authorization': 'Basic {}'.format(
                    base64.b64encode(
                        '{}:{}'.format(self.username, self.api_token)
                    )
                ),
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            params={"username": username}
        )

        result = None
        try:
            result = json.loads(response.text)
        except:
            print "Failed to parse Confluence page response:"
            print response

            return None

        if "userKey" not in result:
            print "Count not find userKey for {}".format(username)

            return None

        return result["userKey"]
