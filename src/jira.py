import json
import requests
import base64

from sets import Set
from time import sleep
from datetime import *
from heapq import heappush, heappop
from dateutil.parser import parse
from requests.auth import HTTPBasicAuth

class Client(object):
    def __init__(
        self,
        username,
        api_token,
        triage_ticket_query,
        release_ticket_query
    ):
        self.username = username
        self.api_token = api_token
        self.triage_ticket_query = triage_ticket_query
        self.release_ticket_query = release_ticket_query

    def get_release_tickets(self):
        try:
            result_text = requests.post(
                "https://oandacorp.atlassian.net/rest/api/2/search",
                headers={
                    'Authorization': 'Basic {}'.format(
                        base64.b64encode(
                            '{}:{}'.format(self.username, self.api_token)
                        )
                    ),
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                data=json.dumps(
                    {
                        "jql": self.release_ticket_query
                    }
                )
            ).text
        except Exception as e:
            print "Failed to retrieve release tickets from JIRA."
            print str(e)

            return None, None

        try:
            result = json.loads(result_text)
        except:
            print "Failed to parse JIRA releases response:"
            print result_text

            return None, None

        if "issues" not in result:
            print "No issues field found in response from JIRA."

            return None, None

        tickets_by_status = {}
        tickets_by_assignee = {}

        for issue in result["issues"]:
            if "fields" not in issue:
                print "Fields not found in {}".format(
                    json.dumps(issue, indent=4)
                )

                continue

            fields = issue["fields"]

            if "customfield_14701" not in fields:
                print "Raven status not found in {}".format(
                    issue["key"]
                )

                continue

            if fields["customfield_14701"] is None:
                print "Raven status not set in {}".format(
                    issue["key"]
                )

                continue

            if "customfield_14800" not in fields:
                print "Raven link not found in {}".format(
                    issue["key"]
                )

                continue

            jira_id = issue["key"]
            jira_link = "https://oandacorp.atlassian.net/browse/{}".format(
                jira_id
            )
            raven_link = fields["customfield_14800"]

            if raven_link is None:
                print "Raven link not set in {}".format(
                    issue["key"]
                )

                continue

            raven_links = raven_link.split("\n")
            raven_ids = [link[link.rfind("=") + 1:] for link in raven_links]

            raven_statuses = fields["customfield_14701"]
            if not raven_statuses:
                print "Raven status not set in {}".format(
                    issue["key"]
                )

                continue

            if "assignee" not in fields or fields["assignee"] is None:
                continue

            assignee = fields["assignee"]["name"]

            print raven_statuses
            print raven_ids

            count = 0
            for raven_id in raven_ids:
                if count >= len(raven_statuses):
                    break

                print "Somehow there were too many raven_ids? ", raven_id, raven_ids, count
                status_tokens = raven_statuses[count].split("_")

                if len(status_tokens) != 3:
                    print "The raven status is weird: {}".format(
                        raven_statuses[count]
                    )

                    continue

                status = status_tokens[2]

                if status not in tickets_by_status:
                    tickets_by_status[status] = {}

                tickets_by_status[status][raven_id] = {
                    "status": status,
                    "environment": status_tokens[1],
                    "raven_link": raven_links[count][raven_links[count].find('http'):],
                    "jira_id": jira_id,
                    "jira_link": jira_link,
                    "summary": fields["summary"],
                    "assignee": assignee
                }
                count += 1

                if assignee not in tickets_by_assignee:
                    tickets_by_assignee[assignee] = {}

                tickets_by_assignee[assignee][raven_id] = (
                    tickets_by_status[status][raven_id]
                )

        return tickets_by_status, tickets_by_assignee

    def get_triage_tickets(self):
        result = json.loads(
            requests.post(
                "https://oandacorp.atlassian.net/rest/api/2/search",
                headers={
                    'Authorization': 'Basic {}'.format(
                        base64.b64encode(
                            '{}:{}'.format(self.username, self.api_token)
                        )
                    ),
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                data=json.dumps(
                    {
                        "jql": self.triage_ticket_query
                    }
                )
            ).text
        )

        if "issues" not in result:
            print "No issues field found in response from JIRA."

            return None

        tickets_by_status = {}
        tickets_by_assignee = {}

        for issue in result["issues"]:
            if "fields" not in issue:
                print "Fields not found in {}".format(
                    json.dumps(issue, indent=4)
                )

                continue

            fields = issue["fields"]

            if "status" not in fields:
                print "status not found in {}".format(
                    json.dumps(issue, indent=4)
                )

                continue

            status = fields["status"]["name"]

            if status not in tickets_by_status:
                tickets_by_status[status] = {}

            tickets_by_status[status][issue["key"]] = issue

            if "assignee" not in fields or fields["assignee"] is None:
                continue

            assignee = fields["assignee"]["name"]

            if assignee not in tickets_by_assignee:
                tickets_by_assignee[assignee] = {}

            tickets_by_assignee[assignee][issue["key"]] = issue

        return tickets_by_status, tickets_by_assignee
