from __future__ import print_function
import httplib2
import os

from apiclient.discovery import build
from oauth2client import client
from oauth2client import tools
from oauth2client.file import Storage

import datetime

# If modifying these scopes, delete your previously saved credentials
# at ~/.credentials/calendar-python-quickstart.json

class Client(object):
    def __init__(
        self,
        application,
        client_secret_filepath,
        credential_filepath
    ):
        self.application = application

        self.credentials = self.get_credentials(
            client_secret_filepath,
            credential_filepath
        )
        self.service = build(
            'calendar',
            'v3',
            http=self.credentials.authorize(httplib2.Http())
        )

    def get_credentials(self, client_secret_filepath, credential_filepath):
        """Gets valid user credentials from storage.

        If nothing has been stored, or if the stored credentials are invalid,
        the OAuth2 flow is completed to obtain the new credentials.

        Returns:
            Credentials, the obtained credential.
        """
        store = Storage(credential_filepath)
        credentials = store.get()
        if not credentials or credentials.invalid:
            flow = client.flow_from_clientsecrets(
                client_secret_filepath,
                'https://www.googleapis.com/auth/calendar.readonly'
            )

            credentials = tools.run_flow(flow, store)

            print('Storing credentials to ' + credential_filepath)

        return credentials

    def get_vacationers(self):
        now = datetime.datetime.utcnow().isoformat() + 'Z'

        eventsResult = self.service.events().list(
            calendarId='primary',
            timeMin=now,
            maxResults=10,
            singleEvents=True,
            orderBy='startTime'
        ).execute()

        events = eventsResult.get('items', [])

# def main():
#     """Shows basic usage of the Google Calendar API.
# 
#     Creates a Google Calendar API service object and outputs a list of the next
#     10 events on the user's calendar.
#     """
#     print('Getting the upcoming 10 events')
# 
#     if not events:
#         print('No upcoming events found.')
#     for event in events:
#         start = event['start'].get('dateTime', event['start'].get('date'))
#         print(start, event['summary'])
# 
# 
# if __name__ == '__main__':
#     main()
