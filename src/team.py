import json
import requests

from sets import Set
from time import sleep
from datetime import *
from heapq import heappush, heappop
from dateutil.parser import parse

class Team(object):
    class Person(object):
        def __init__(
            self,
            first_name,
            last_name,
            slack_name,
            phabricator_name,
            gender
        ):
            self.first_name = first_name
            self.last_name = last_name
            self.slack_name = slack_name
            self.phabricator_name = phabricator_name
            self.gender = gender

        def __str__(self):
            return "{} {}|{}|{}".format(
                self.first_name,
                self.last_name,
                self.slack_name,
                self.phabricator_name
            )

    def __init__(self, members, reference_date):
        self.members = {
            member['slack_name']: Team.Person(
                member['first_name'],
                member['last_name'],
                member['slack_name'],
                member['phabricator_name'],
                member['gender']
            )
            for member in members
        }

        self.reference_date = reference_date
        self.triage_order = [
            member
            for member in members
            if 'exclude_from_triage' not in member or
                not member['exclude_from_triage']
        ]
        self.triage_person_index = (
            ((datetime.today() - reference_date).days / 7) %
            len(self.triage_order)
        )
        self.triage_person = (
            self.triage_order[self.triage_person_index]['slack_name']
        )

        print "Triage order:", self.triage_order

    def update_triage_person(self, vacationers):
        self.triage_person_index = (
            ((datetime.today() - self.reference_date).days / 7) %
            len(self.triage_order)
        )
        self.triage_person = (
            self.triage_order[self.triage_person_index]['slack_name']
        )

        # next_triage_person_index = self.triage_person_index
        # while self.triage_persion in vacationers:
        #     next_triage_person_index = (
        #         (next_triage_person_index + 1) % len(self.members)
        #     )
        #     self.triage_order[self.triage_person_index],
        #     self.triage_order[next_triage_persion_index] =
        #     self.triage_order[next_triage_persion_index],
        #     self.triage_order[self.triage_person_index]

    def get_member_by_phabricator_name(self, phabricator_name):
        for member in self.members.values():
            if member.phabricator_name == phabricator_name:
                return member

        return None

    def get_triage_schedule(self):
        schedule = []
        current_date = date.today()
        current_week_start = current_date - timedelta(current_date.weekday())
        for week in range(0, 12):
            week_start = current_week_start + timedelta(7 * week)

            username = self.triage_order[
                ((week_start - self.reference_date.date()).days / 7) %
                len(self.triage_order)
            ]['phabricator_name']

            schedule.append(
                {
                    "username": username,
                    "week_start": week_start
                }
            )

        return schedule

    def get_slack_names(self, triage_only):
        return (
            [member for member in self.members.items()]
            if not triage_only else
            [member['slack_name'] for member in self.triage_order]
        )

