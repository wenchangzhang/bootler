PACKAGES := bootler.pkg

bootler.pkg_DEPS := $(BOOTLER_VENV_BUILD)
bootler.pkg_ARGS := -D BOOTLER_TOP=$(BOOTLER_TOP) \
                              -D PYTHON_VERSION=$(PYTHON_VERSION)
